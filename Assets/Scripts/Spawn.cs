﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    public List<GameObject> obstaculos;

    public GameObject banana;
    public GameObject garbage;
    public GameObject soda;

    private float accTime;
    public float tempoSpawn;

    // Use this for initialization
    void Start()
    {
        obstaculos = new List<GameObject> { banana, garbage, soda };
    }

    // Update is called once per frame
    void Update()
    {
        accTime += Time.deltaTime;
        if (accTime >= tempoSpawn)
        {
            accTime = 0;
            Instantiate(obstaculos[Random.Range(0, 3)]);
        }
    }
}
